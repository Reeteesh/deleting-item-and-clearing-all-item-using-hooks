import React from "react";
import data from "./data";

const App = () => {
  const [register, setRegister] = React.useState(data);
  const displayData = register.map((item) => {
    const { id, name } = item;
    return (
      <>
        <div key={id}>
          <h1>{name}</h1>
          <button
            onClick={() => {
              deleteItem(id);
            }}
          >
            Remove
          </button>
        </div>
      </>
    );
  });
  const deleteItem = (id) => {
    setRegister((oldregister) => {
      const newRegister = oldregister.filter((x) => x.id !== id);
      return newRegister;
    });
  };
  return (
    <div>
      {displayData}

      <button
        onClick={() => {
          setRegister([]);
        }}
      >
        clear List
      </button>
    </div>
  );
};

export default App;
